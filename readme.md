# NR React Starter

> My official react starter command for creating new React App based on my personal requirements.

## What Will Installed

At the first time, this starter command will generate:
* The CRA (Create React App) bundler.
* React Router.
* React Redux
* GraphQL library.
* Axios HTTP Client library.
* Base structure:
    * All CRA base structure.
    * `config`: Holds all external **Webpack Configuration** without ejecting the CRA.
    * `src`:
        * `actions`: This will hold all action scripts.
        * `classes`: All class scripts or internal libraries.
        * `components`: All UI components.
        * `graphqls`: All data source in **GraphQL** format.
        * `layouts`: All layout components.
        * `pages`: All page components.
        * `reducers`: All redux reducers.
        * `resources`:
            * `css`: All css regarding the global UI/theme.
            * `static`: All static files, e.g. images, fonts, etc.
        * `app.jsx`: The base of App script.
        * `routes.jsx`: Routing script.
    * `tests`:
        * `automation`: For all automation test scripts with **Mocha + Chai HTTP**.
        * `e2e`: For all end 2 end test scripts with **CodeceptJS**. I use `puppeteer` as my headlesss browser engine.
    * `.env.example`: For project environment configurations.
    * `codecept.json`: The initial codecept configuration.
    * `config.js`: This is the script that make me can extend webpack configuration without ejecting the CRA.
* Test Case Tools:
    * `mocha`
    * `chai + chaiHttp`
    * `codeceptjs`
    * `jest`

## Commands

Below are the commands for developing the app/website:
* All CRA commands.
* `npm run e2e`: Run **End 2 End Test** using **CodeceptJS**.
* `npm run automation`: Run **Automation Test** using **Mocha, Chai** and **Chai HTTP**.

## Getting Started

### Pre-Requisites

* Node JS
* [Create React App](https://facebook.github.io/create-react-app/).
* Basic Javascript knowledge.
* Basic React JS knowledge.
* Basic Automated Test knowledge.
* Basic End 2 End Test knowledge.
* Basic UI Test (Jest) knowledge.
* Some ☕ and 😎 🌴.

### Installation
* Open terminal.
* Run `npm install -g @nurimansyah/react-starter`. (With sudo if you install Node JS as your global application. UNIX Only)
* To create new project, run `nr create <destination/folder>`.
* And then, you can use all the commands above.

## Extending Webpack Configuration
If you want to extends your webpack configuration, just look at `config` folder, and make some configuration regarding your current environment:
* `config/development.js`: Is use for extending webpack configuration in development mode.
* `config/production.js`: Is use for extending webpack configuration in production mode.
* `config/testing.js`: Is use for extending webpack configuration in testing mode.

## Contributing

See the [contributing.md](./contributing.md) for more information.

## License

See the [license.md](./license.md) for more information.

## Changelog

See the [changelog.md](./changelog.md) for more information.

## Caveats

* Currently I don't have support to Windows OS, Sorry... I'll update this in the future request.
* For deploying to your own server, if you use Apache or NginX web server, make sure you have rewrite the path using `.htaccess` or in NginX server configuration.

- - -

Regards,  
**Nurimansyah Rifwan**