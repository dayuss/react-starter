# Contribution Guidelines

## Versioning

I'm using [Semver](https://semver.org) for versioning system.

## Testing
* `npm test`: To start all unit testing.

## Developing

* Fork this repo.
* Run `npm install`.
* Run `npm link`. (For linking this project binary to your app. **NOTE: If you have install the `@nurimansyah/react-starter` script before, make sure you uninstall it first**)
* And, this project ready for improvement.

## Use Gitlab Issue for Any Question
If you have any question or more feedback, please use this repo issue system [here](https://gitlab.com/nr-osp/react-starter/issues).

All issue may used for referencing the future enhancements.

## Merge Request

* After you have any changes, fix or updates, just use **Merge Request** tool [here](https://gitlab.com/nr-osp/react-starter/merge_requests).
* I will review the changes and approve or reject your changes.

## Personal Notes
I was open for discussion regarding this project. You may drop me a message to nurimansyah.rifwan@gmail.com, or use the gitlab issue system.

- - -
**Nurimansyah Rifwan**