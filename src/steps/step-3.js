const path = require('path');
const chalk = require('chalk');
const log = console.log;
const { msg } = require('../modules/utils');
const updateJSON = require('update-json');

module.exports = (response, destination) => {

    log(chalk.green(msg.separator()));
    log(chalk.green('CRA Installed, cleaning up...'));

    const pkg = path.resolve(destination, 'package.json');
    
    // Get package.json
    let package = require(pkg);

    // First, we need to change the package.json description
    package.name = response.name;
    package.description = response.description;

    // Second, update the scripts
    package.scripts.start = 'node config start';
    package.scripts.build = 'node config build';
    package.scripts.test = 'node config test';
    package.scripts.e2e = 'mocha tests/automation';
    package.scripts.automation = 'codeceptjs run tests/e2e --steps';
    package.scripts.styleguide = 'styleguidist server';
    package.scripts['styleguide:build'] = 'styleguidist build';

    // Then update the package.json
    const doUpdate = new Promise((resolve, reject) => {
        updateJSON(pkg, package, err => {
            if (err) reject(err);
            else resolve();
        });
    });

    // Return
    return doUpdate;
};