const path = require('path');
const rimraf = require('rimraf');
const copyDir = require('copy-dir');
const log = console.log;
const chalk = require('chalk');
const { msg } = require('../modules/utils');

module.exports = destination => new Promise((resolve, reject) => {

    log(chalk.green(msg.separator()));
    log(chalk.green('Removing "src" folder...'));

    // Next, remove "src" folder
    rimraf(path.resolve(destination, 'src'), err => {
        if (err) reject(err);
        else log(chalk.green('The "src" removed.'));
    });

    // Remove .git folder
    rimraf(path.resolve(destination, '.git'), err => {
        if (err) reject(err);
        else log(chalk.green('The ".git" removed.'));
    });

    // Remove readme.md folder
    rimraf(path.resolve(destination, 'readme.md'), err => {
        if (err) reject(err);
        else log(chalk.green('The "readme.md" removed.'));
    });

    log(chalk.green(msg.separator()));
    log(chalk.green('Copying templates...'));

    // Then we copy the template from "templates" folder
    const tpl = path.resolve(__dirname, '../templates');
    copyDir(tpl, path.resolve(destination), err => {
        if (err) reject(err);
        else {
            log(chalk.green('All templates copied.'));
            resolve();
        }
    });

});