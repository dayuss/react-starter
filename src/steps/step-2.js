const shellExec = require('shell-exec');
const log = console.log;
const chalk = require('chalk');
const { msg } = require('../modules/utils');

module.exports = (dest, isCRA) => {

    if (!isCRA) {
        log(chalk.green(msg.separator()));
        log(chalk.green.bold('Installing Create React Application...'));
    }

    return shellExec(`create-react-app ${dest}`);
};