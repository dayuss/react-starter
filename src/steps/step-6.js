const path = require('path');
const log = console.log;
const chalk = require('chalk');
const { msg } = require('../modules/utils');
const shellExec = require('shell-exec');

module.exports = destination => new Promise((resolve, reject) => {

    log(chalk.green(msg.separator()));
    log(chalk.green('Installing dependencies...'));

    /**
     * Dependencies List
     * - Babel Polyfill
     * - React Router
     * - React Router DOM
     * - React Redux
     * - React Loadable
     * - ImmutableJS
     * - Redux
     * - Redux Thunk
     * - GraphQL
     * - Axios
     * - Rewire
     * - Proxyquire
     */
    const dependencies = 'babel-polyfill react-router react-router-dom react-redux redux-thunk react-loadable immutable redux redux-thunk graphql axios rewire proxyquire';

     /**
      * Dev Dependencies List
      * - Styleguidist
      * - Node SASS
      * - Webpack Notifier
      * - Mocha
      * - Chai + Chai HTTP
      * - Codecept + Puppeteer
      */
    const devDependencies = 'react-styleguidist node-sass webpack-build-notifier codeceptjs puppeteer mocha chai chai-http';

    // Install dependencies
    const installDependencies = () => shellExec(`cd ${path.resolve(destination)} && npm i -S ${dependencies}`);
    // Install development dependencies
    const installDevDependencies = () => shellExec(`cd ${path.resolve(destination)} && npm i -D ${devDependencies}`);

    // Start
    installDependencies()
        .then(() => {
        log(chalk.green('All dependencies installed.'));
        return installDevDependencies();
    })
        .then(() => {
            log(chalk.green('All development dependencies installed.'));
            resolve();
        });
});