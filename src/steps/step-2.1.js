const shellExec = require('shell-exec');
const log = console.log;
const chalk = require('chalk');

module.exports = () => {

    log(chalk.green('Looks like you don\'t have "Create React App" installed. Installing CRA..'));

    // Install CRA
    return shellExec('npm i -g create-react-app');
};