const log = console.log;
const chalk = require('chalk');
const { msg } = require('../modules/utils');

module.exports = () => {
    log(chalk.green(msg.separator()));
    log(chalk.green('You website has been successfully initiated!'));
};