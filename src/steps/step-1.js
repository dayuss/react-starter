const path = require('path');
const chalk = require('chalk');
const log = console.log;
const { msg } = require('../modules/utils');
const { prompt } = require('enquirer');
const pkg = require(path.resolve(__dirname, '../../package.json'));

module.exports = () => {
    log(chalk.green(msg.separator()));
    log(chalk.green.bold(`Welcome to NR React Starter! v${pkg.version}`));
    log(chalk.green(msg.separator()));

    log(chalk.green('Please follow this wizard to initiate your website/app:'));

    const response = prompt([
        {
            type: 'input',
            name: 'name',
            message: 'Type your project name:'
        },
        {
            type: 'input',
            name: 'description',
            message: 'Describe your project description:'
        }
    ]);
    return response;
};