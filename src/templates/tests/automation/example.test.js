const chai = require('chai');
const should = require('chai').should();
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

describe('Example Test', () => {
    it('Should return true', () => {
        const $var = true;
        $var.should.be.equal(true);
    });
});

describe('Example HTTP Automation Test', () => {
    it('Should return 200 HTTP status', done => {
        chai.request('https://google.com')
            .get('/')
            .end((err, res) => {
                if (err) done(err);

                res.status.should.be.equal(200);
                done()
            });
    });
});