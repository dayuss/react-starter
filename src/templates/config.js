const rewire = require('rewire');
const proxyquire = require('proxyquire');

switch(process.argv[2]) {
    case 'start':
      rewireModule('react-scripts/scripts/start.js', loadCustomizer('./config/development'));
      break;
    case 'build':
      rewireModule('react-scripts/scripts/build.js', loadCustomizer('./config/production'));
      break;
    case 'test':
      let customizer = loadCustomizer('./config/testing');
      proxyquire('react-scripts/scripts/test.js', {
        '../utils/createJestConfig': (...args) => {
          var createJestConfig = require('react-scripts/utils/createJestConfig');
          return customizer(createJestConfig(...args));
        }
      });
      break;
    default:
      console.log('customized-config only supports "start", "build", and "test" options.');
      process.exit(-1);
  }
  
  function loadCustomizer(module) {
    try {
      return require(module);
    } catch(e) {
      if(e.code !== "MODULE_NOT_FOUND") {
        throw e;
      }
    }
    return config => config;
  }
  
  function rewireModule(modulePath, customizer) {
    let defaults = rewire(modulePath);
    let config = defaults.__get__('config');
    customizer(config);
  }