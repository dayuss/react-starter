const Notify = require('webpack-build-notifier');

module.exports = config => {
    // External config configuration
    // e.g. config.plugins

    // Notifier Plugins
    config.plugins.push(new Notify({
        title: 'React App Build'
    }))
};