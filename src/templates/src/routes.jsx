import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

// Import your pages component here...

export default () => (
    <Router>
        <Switch>
            <Route exact path="/" render={() => (
                <div>
                    <h1>Congratulations!</h1>
                    <p>You've successfully setup your React project!</p>
                    <p>For routing test:</p>
                    <ul>
                        <li><Link to="/about">About Page</Link></li>
                    </ul>
                </div>
            )} />
            <Route path="/about" render={() => (
                <div>
                    <h1>NR React Starter</h1>
                    <p>This starter it based on my personal use. But you can edit as much as you want.</p>

                    <ul>
                        <li><Link to="/">Home</Link></li>
                    </ul>
                </div>
            )} />
        </Switch>
    </Router>
);