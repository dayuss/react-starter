#!/usr/bin/env node

'use strict';

const path = require('path');
const app = require('commander');
const pkg = require(path.resolve(__dirname, '../package.json'));

const steps = {
    welcome: require('./steps/step-1'),
    installCRA: require('./steps/step-2.1'),
    createNewProject: require('./steps/step-2'),
    cleaningCRA: require('./steps/step-3'),
    copyTemplate: require('./steps/step-4'),
    finishUp: require('./steps/step-5'),
    installDependencies: require('./steps/step-6')
};

let response = {};
let destination = '';

// App Configuration
app
    .version(pkg.version);

// Create Commands
app
    .command('create <dest>')
    .description('Create a new React App with starter.')
    .action(dest => {
        steps
            .welcome()
            .then(resp => {
                response = resp;
                destination = dest;
                return steps.createNewProject(dest);
            })
            .then(resp => {
                if (resp.stderr) return steps
                    .installCRA()
                    .then(() => steps.createNewProject(destination, true))
                    .then(() => steps.cleaningCRA(response, destination));
                else return steps.cleaningCRA(response, destination);
            })
            .then(() => steps.installDependencies(destination))
            .then(() => steps.copyTemplate(destination))
            .then(steps.finishUp)
            .catch(err => console.error(err.message));
    });

// Run App
app.parse(process.argv);