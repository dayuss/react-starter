# Changelog

## 30 November 2018
* Added default Redux and Router implementation
* Add `resources` folder for `css` and `static` files.

## 28 November 2018
* Initial commit.
* Added `color` and `commander` as dependencies.
* Added `mocha` and `chai` as dev-dependencies.
* Create new `index.js` and test to console log.