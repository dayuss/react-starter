'use strict';

const chai = require('chai');
const should = chai.should();
const { msg } = require('../src/modules/utils');

/**
 * Module init
 */
describe('Module Init:', () => {
    it('Should have "msg" property.', () => {
        msg.should.be.an('object');
    });
});

/**
 * Message Test
 */
describe('Message Test:', () => {
    it('Should have "separator" function', () => {
        msg.separator.should.be.a('function');
    });
});